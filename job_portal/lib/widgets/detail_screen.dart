import 'package:flutter/material.dart';
import 'package:job_portal/job_model.dart';

class DetailScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    final Job job =  ModalRoute.of(context).settings.arguments;
    // Use the Todo to create the UI.
    return Scaffold(
      appBar: AppBar(
        title: Text(job.title),
      ),
      body: 
        SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
          child: Column(
            children: <Widget>[
                Row(
                    children: <Widget>[
                      Text('Job Title: ', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16),),
                      Flexible(child: Text(job.title ?? 'Not Provided', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),),
                      ),
                    ],
                ),
                Row(
                  children: <Widget>[
                    Text('Company Name: ', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16),),
                    Text(job.company ?? 'Not Provided', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),),
                  ],
                ),
                Image.network(job.companyLogo,
                     width: 300, height: 200, fit: BoxFit.contain,),
                
                Column(
                  children: <Widget>[
                    Text('Job Description', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18),),
                    Text(job.description)
                  ],
                ),
                
            ],
          ),
      ),
    );
  }
}