import 'dart:convert';

import 'package:job_portal/web_services.dart';

class Job {
  
  final String id; 
  final String type; 
  final String url; 
  final String createdAt; 
  final String company; 
  final String companyUrl;
  final String location;
  final String title;
  final String description;
  final String howToApply;
  final String companyLogo;      
  
  Job({this.id, this.type, this.url, this.createdAt, this.company, this.companyUrl, this.location,
  this.title, this.description, this.howToApply, this.companyLogo});

  factory Job.fromJson(Map<String,dynamic> json) {
    return Job(
      
      id: json['id'], 
      type: json['type'], 
      url: json['url'], 
      createdAt: json['created_at'], 
      company: json['company'], 
      companyUrl: json['companyUrl'], 
      location: json['location'], 
      title: json['title'], 
      description: json['description'], 
      companyLogo: json['company_logo'],
    );
  }

  static String jobUrl = 'http://5e47b61a.ngrok.io/jobs/?limit=20';

  static Resource<List<Job>> getJobs() {
    // not return when null
    if(jobUrl != 'null'){
    return Resource(
      url: jobUrl,
      parse: (response) {
        final result = json.decode(response.body); 
        jobUrl = result['next'];
        Iterable list = result['results'];
        return list.map((model) => Job.fromJson(model)).toList();
      }
    );
  }
  }
}
