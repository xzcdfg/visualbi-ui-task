import 'package:flutter/material.dart';
import 'package:job_portal/job_model.dart';
import 'package:job_portal/web_services.dart';
import 'package:job_portal/widgets/detail_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: JobList(),
    );
  }
}

class JobList extends StatefulWidget {
  JobList({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  JobListState createState() => JobListState();
}

class JobListState extends State<JobList> {
  List<Job> _jobs = new List<Job>();
  ScrollController _scrollController = new ScrollController();

  @override
  void initState(){
    super.initState();
    _populateJobs();

    _scrollController.addListener((){
      print('data');
      if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent){
        _populateJobs();
      }
    });
  }
  
  @override
  void dispose(){
    super.dispose();
    _scrollController.dispose();
  }

  void _populateJobs(){
    Webservice().load(Job.getJobs()).then((jobs) => {
      setState(() => {
        _jobs.addAll(jobs)
      })
    });
  }
  
  ListTile _buildItemsForListView(BuildContext context, int index){
    return ListTile(
      title:Text(_jobs[index].title),
      onTap: (){
        Navigator.push(
          context, 
          MaterialPageRoute(builder: (context)=> DetailScreen(),
          settings: RouteSettings(arguments: _jobs[index]),
        ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Jobs')
      ),
      body: ListView.builder(
        controller: _scrollController,
        itemCount: _jobs.length,
        itemBuilder: _buildItemsForListView,
      ),
    );
  }
}


